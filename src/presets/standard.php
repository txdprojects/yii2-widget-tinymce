<?php
return [
	// Plugins
  'plugins' => 'paste autoresize code print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help filemanager responsivefilemanager',

	// Toolbars
	'toolbar1' => 'formatselect fontsizeselect | bold italic strikethrough forecolor backcolor | link image media responsivefilemanager | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent | removeformat',

	// Configuration
	'autoresize_on_init' => true,
	'autoresize_min_height' => 150,
	'autoresize_bottom_margin' => 5,
  'image_advtab' => true,
  'paste_data_images' => true,
];
