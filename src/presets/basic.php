<?php
return [
	// Plugins
	'plugins' => 'paste autoresize searchreplace autolink directionality fullscreen image link media table charmap hr anchor advlist lists textcolor wordcount imagetools contextmenu colorpicker filemanager responsivefilemanager',

	// Toolbars
	'toolbar1' => 'formatselect fontsizeselect | bold italic forecolor backcolor | link image media responsivefilemanager | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent | removeformat',

	// Configuration
	'menubar' => false,
	'autoresize_on_init' => true,
	'autoresize_min_height' => 150,
	'autoresize_bottom_margin' => 5,
  'image_advtab' => true,
  'paste_data_images' => true,
];
