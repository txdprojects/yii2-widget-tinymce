<?php

namespace txd\widgets\tinymce;

use Yii;
use yii\web\AssetBundle;

class TinyMCEEditorAsset extends AssetBundle
{
	/**
	 * @inheritdoc
	 */
	public $css = [
		'css/editor.css',
	];

	/**
	 * @inheritdoc
	 */
	public $js = [
		'filemanager' => 'plugins/filemanager/plugin.min.js',
		'responsivefilemanager' => 'plugins/responsivefilemanager/plugin.min.js',
	];

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();

		$this->sourcePath = __DIR__ . '/assets';
	}
}
